package com.xingfly.constant;

/**
 * 消息队列的名称->例子中的仓库名(不同类型的数据需要放到不同的仓库)
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
public class MQConstant {
    public static final String NOTIFICATION_MQ = "NOTIFICATION_MQ";
    public static final String FEED_MQ = "FEED_MQ";
    public static final String EVENT_IMAGE_MQ = "EVENT_IMAGE_MQ";
}
