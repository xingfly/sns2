package com.xingfly.constant;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
public class NotificationStatus {
    public static int IS_READ = 1;
    public static int NOT_READ = 0;
}
