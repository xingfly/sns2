package com.xingfly.constant;

/**
 * Created by SuperS on 21/05/2017.
 *
 * @author SuperS
 */
public class RoleConstant {
    public static final String ROLE_USER = "user";
    public static final String ROLE_ADMIN = "admin";
}
