package com.xingfly.constant;


/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
public class Type {
    //消息类型
    public static final int NOTIFICATION_POST = 0;
    public static final int NOTIFICATION_COMMENT = 1;
    public static final int NOTIFICATION_REPLY = 2;

    //事件类型
    public static final int EVENT_POST = 0;
    public static final int EVENT_COMMENT = 1;
    public static final int EVENT_REPLY = 2;
    public static final int EVENT_IMAGE = 3;
    //计数器类型
    public static final int COUNT_LIKE = 0;
    public static final int COUNT_COMMENTS = 1;
    //评论类型
    public static final int COMMENT_COMMENT = 0;
    public static final int COMMENT_REPLY = 1;

}
