package com.xingfly.constant;

/**
 * Created by SuperS on 19/05/2017.
 *
 * @author SuperS
 */
public class HttpConstant {
    //token超时时间3小时
    public static final int TOKEN_EXPIRES_HOUR = 3;
    public static final String AUTHORIZATION = "authorization";
    public static final String CURRENT_USER_ID = "current_user_id";
}
