package com.xingfly.constant;

/**
 * Created by SuperS on 24/05/2017.
 *
 * @author SuperS
 */
public class QiNiuConstant {
    public static final String AK = "0ZFldeQZaHezSTbJhwg85hPJ3b-C2j24EgK6kJbM";
    public static final String SK = "BFha_WETDK7AlgN8v84AlWMeRfMhhv8HdKaqAy9n";
    //空间名称
    public static final String BUCKET = "xingfly";
    public static final String DOMAIN = "http://pic.xingfly.com/";
    //1个MB的值
    public static final long MB = 1024 * 1024;
    //单文件限制大小为2MB
    public static final long FILE_SIZE = 2 * MB;
    //一次上传文件不得超过 20MB
    public static final long ALL_FILE_SIZE = FILE_SIZE * 10 * MB;
}
