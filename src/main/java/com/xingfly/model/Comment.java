package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 16/05/2017.
 *
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User author;
    @Column(nullable = false)
    private String content;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Event event;
    @JoinColumn(name = "id")
    private Long parentId;
    @Column(nullable = false)
    private Integer type;
    @Column(nullable = false, updatable = false)
    private Date createTime;

    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }


    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", author=" + author +
                ", content='" + content + '\'' +
                ", event=" + event +
                ", parentId=" + parentId +
                '}';
    }
}
