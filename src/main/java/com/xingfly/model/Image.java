package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Image implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;
    //图片的地址、采用的是七牛 这里是 图片在七牛的链接
    private String url;
    //图片加密后的值 唯一 如果检查到 hash已存在就不再上传 而是 直接 创建一个对象 赋值为 已有url
    private String hash;

    //Image和事件的关联。比如用户发送了Post包含 post对应的事件就是Event。
    // 和Event关联就可以把用户发表的图片和Post关联起来形成Feed流也就是我们的动态列表包含了好友发布的post
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "image_event",
            joinColumns = {@JoinColumn(name = "image_id",referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "event_id",referencedColumnName = "id")},
            uniqueConstraints = @UniqueConstraint(columnNames = {"event_id", "image_id"})
    )
    private List<Event> events;

    @Column(nullable = false, updatable = false)
    private Date createTime;

    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }
}
