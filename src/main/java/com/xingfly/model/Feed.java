package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 14/05/2017.
 *  feed流 动态
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Feed implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //Feed的创造者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User author;
    //Feed的接受者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User fans;
    //有关的事件(比如发表动态)
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Event event;
    //创建时间
    @Column(nullable = false,updatable = false)
    private Date createTime;
    //自动初始化创建时间
    @PrePersist
    public void prePersist() {
        this.createTime = new Date();
    }

}
