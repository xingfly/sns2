package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 12/05/2017.
 * 朋友关系
 * @author SuperS
 */
@Entity
@Getter
@Setter
@Table(name = "friend", uniqueConstraints = {@UniqueConstraint(columnNames = {"start_id", "fans_id"})})
public class Friend implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //被追随
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User start;
    //追随者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User fans;

    @Column(nullable = false, updatable = false)
    private Date createTime;

    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }


}
