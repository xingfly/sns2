package com.xingfly.model;

import com.xingfly.constant.NotificationStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 12/05/2017.
 * 消息提醒
 *
 * @author SuperS
 */
@Getter
@Setter
@Entity
public class Notification implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //消息类型 POST、COMMENT、REPLY
    @Column(nullable = false)
    private int type;
    //消息关联的事件
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private Event event;
    //消息的发送者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private User sender;
    //消息的接受者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private User receiver;
    //消息已读或者未读(默认未读)
    @Column(nullable = false)
    private Integer status = NotificationStatus.NOT_READ;
    //创建时间
    @Column(nullable = false, updatable = false)
    private Date createTime;
    //自动初始化创建时间
    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }

}
