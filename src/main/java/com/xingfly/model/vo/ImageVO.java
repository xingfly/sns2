package com.xingfly.model.vo;

import com.xingfly.model.Image;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by SuperS on 25/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class ImageVO implements Serializable {
    private Long id;
    private String url;

    public static ImageVO toVO(Image image) {
        ImageVO imageVO = new ImageVO();
        imageVO.setId(image.getId());
        imageVO.setUrl(image.getUrl());
        return imageVO;
    }
}
