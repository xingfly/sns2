package com.xingfly.model.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by SuperS on 16/05/2017.
 * 计数展示对象
 *
 * @author SuperS
 */
@Getter
@Setter
public class CountVO {
    private Long id;
    private Long likeCount;
    //喜欢人数
    private List<UserVO> users;
}
