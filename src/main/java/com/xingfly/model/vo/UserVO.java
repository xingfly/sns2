package com.xingfly.model.vo;

import com.xingfly.model.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

/**
 * Created by SuperS on 18/05/2017.
 * 用户展示对象
 *
 * @author SuperS
 */
@Getter
@Setter
public class UserVO {
    private Long id;
    private String username;

    public static UserVO toVO(User user) {
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        return userVO;
    }
}
