package com.xingfly.model.vo;

import com.xingfly.model.Notification;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class NotificationVO {
    private Long id;
    private UserVO receiver;
    private UserVO sender;
    private Integer type;
    private Integer read;
    private NotificationEventVO eventVO;
    private Date createTime;

    public static NotificationVO toVO(Notification n) {
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setId(n.getId());
        notificationVO.setEventVO(NotificationEventVO.toVO(n.getEvent()));
        notificationVO.setSender(UserVO.toVO(n.getSender()));
        notificationVO.setReceiver(UserVO.toVO(n.getReceiver()));
        notificationVO.setRead(n.getStatus());
        notificationVO.setType(n.getType());
        notificationVO.setCreateTime(n.getCreateTime());
        return notificationVO;
    }
}
