package com.xingfly.model.vo;

import com.xingfly.model.Event;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class NotificationEventVO {
    private Long id;
    private String content;

    public static NotificationEventVO toVO(Event event) {
        NotificationEventVO eventVO = new NotificationEventVO();
        eventVO.setId(event.getId());
        eventVO.setContent(event.getSummary());
        return eventVO;
    }
}
