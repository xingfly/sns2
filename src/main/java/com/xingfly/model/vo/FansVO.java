package com.xingfly.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 22/05/2017.
 * 粉丝展示对象
 * @author SuperS
 */
@Getter
@Setter
public class FansVO {
    private Long id;
    private UserVO userVO;
}
