package com.xingfly.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 16/05/2017.
 * 动态流图片展示对象
 *
 * @author SuperS
 */
@Getter
@Setter
public class FeedImageVO {
    private String url;
}
