package com.xingfly.model.vo;

import com.xingfly.model.Post;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class PostVO implements Serializable {
    private Long id;
    private String content;
    private UserVO userVO;
    private Date createTime;
    private List<ImageVO> imageVOS;

    public static PostVO toVO(Post post) {
        PostVO postVO = new PostVO();
        postVO.setContent(post.getContent());
        postVO.setId(post.getId());
        postVO.setCreateTime(post.getCreateTime());
        postVO.setUserVO(UserVO.toVO(post.getAuthor()));
        return postVO;
    }

    public static PostVO toVO(Post post, List<ImageVO> imageVOS) {
        PostVO postVO = new PostVO();
        postVO.setContent(post.getContent());
        postVO.setId(post.getId());
        postVO.setCreateTime(post.getCreateTime());
        postVO.setUserVO(UserVO.toVO(post.getAuthor()));
        postVO.setImageVOS(imageVOS);
        return postVO;
    }
}
