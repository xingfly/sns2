package com.xingfly.model.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class StarVO {
    private Long id;
    private UserVO userVO;
}
