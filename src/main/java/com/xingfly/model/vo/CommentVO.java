package com.xingfly.model.vo;

import com.xingfly.model.Comment;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by SuperS on 16/05/2017.
 * 评论展示对象
 *
 * @author SuperS
 */
@Getter
@Setter
public class CommentVO {
    private Long id;
    private String content;
    private List<CommentVO> replies;
    private UserVO author;
    private Integer type;
    private Date createTime;

    public static CommentVO toVoIgnoreReplies(Comment c) {
        CommentVO commentVO = new CommentVO();
        commentVO.setId(c.getId());
        commentVO.setCreateTime(c.getCreateTime());
        commentVO.setContent(c.getContent());
        commentVO.setAuthor(UserVO.toVO(c.getAuthor()));
        commentVO.setType(c.getType());
        return commentVO;
    }
}
