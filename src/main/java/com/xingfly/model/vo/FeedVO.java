package com.xingfly.model.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by SuperS on 16/05/2017.
 * 动态展示对象
 * @author SuperS
 */
@Getter
@Setter
public class FeedVO {
    private Long id;
    private UserVO author;
    private String content;
    private List<FeedImageVO> images;
    private CountVO countVO;
    private List<CommentVO> comments;
    private Long feedEventId;
    private Date createTime;
}
