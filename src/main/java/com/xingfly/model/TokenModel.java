package com.xingfly.model;

import java.io.Serializable;

/**
 * Created by SuperS on 19/05/2017.
 *
 * @author SuperS
 */
public class TokenModel implements Serializable {
    private Long id;
    private String token;

    public TokenModel(Long id, String token) {
        this.id = id;
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
