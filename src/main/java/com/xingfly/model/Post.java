package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 12/05/2017.
 * post
 *
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Post implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //post内容
    @Column(nullable = false)
    private String content;

    //作者
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private User author;

    @Column(nullable = false, updatable = false)
    private Date createTime;

    @PrePersist
    public void prePersist() {
        this.createTime = new Date();
    }

    public Post() {
    }
    public Post(String content, User author) {
        this.content = content;
        this.author = author;
    }
}
