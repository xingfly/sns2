package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by SuperS on 15/05/2017.
 * 计数器
 *
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Count implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //计数类型
    @Column(nullable = false)
    private int type;
    //来自哪个用户
    @ManyToMany
    @JoinTable(name = "count_user",
            joinColumns = {@JoinColumn(name = "count_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    private List<User> users;
    //计数对象obj
    @OneToOne
    @JoinColumn(referencedColumnName = "id",unique = true)
    private Event event;
    //喜欢数量
    private Long count;
    //创建日期
    @Column(nullable = false, updatable = false)
    private Date createTime;
    //初始化自动赋值为创建时间
    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }
}
