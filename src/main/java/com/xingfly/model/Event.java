package com.xingfly.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by SuperS on 13/05/2017.
 * 事件
 * @author SuperS
 */
@Entity
@Getter
@Setter
public class Event implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //事件类型 post、comment、reply
    @Column(nullable = false)
    private Integer type;
    //事件简介
    private String summary;
    //事件创建者 一个用户可以创建多个事件
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;
    //事件对象的id 事件包装了 Post Comment 所以 ObjectID就是 具体 post 或者 comment对象的id
    @Column(nullable = false)
    private Long eventObjectId;
    @Column(nullable = false, updatable = false)
    private Date createTime;

    @PrePersist
    public void prePersist() {
        createTime = new Date();
    }

}
