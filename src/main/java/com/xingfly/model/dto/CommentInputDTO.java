package com.xingfly.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 23/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class CommentInputDTO {
    private String content;
    private Long parentId;
    private Long objId;
    private Integer objType;

}
