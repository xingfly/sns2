package com.xingfly.model.dto;

import com.xingfly.model.vo.ImageVO;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by SuperS on 24/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class EventImageDTO implements Serializable {
    private List<ImageVO> imageVOS;
    private Long eventId;
}
