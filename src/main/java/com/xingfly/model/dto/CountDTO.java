package com.xingfly.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 24/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class CountDTO {
    private Long objId;
    private Integer objType;
}
