package com.xingfly.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 30/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
public class AuthorizationDTO {
    private String username;
    private String password;
}
