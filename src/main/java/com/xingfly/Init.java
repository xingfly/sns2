package com.xingfly;

import com.xingfly.constant.RoleConstant;
import com.xingfly.model.Post;
import com.xingfly.model.Role;
import com.xingfly.model.User;
import com.xingfly.model.vo.PostVO;
import com.xingfly.service.FriendService;
import com.xingfly.service.PostService;
import com.xingfly.service.RoleService;
import com.xingfly.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
@Component
public class Init implements CommandLineRunner {
    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;
    @Autowired
    private FriendService friendService;
    @Autowired
    private RoleService roleService;


    @Override
    public void run(String... strings) throws Exception {
        User fpf = new User();
        fpf.setUsername("fpf");
        fpf.setPassword("fpf");
        userService.create(fpf);


        User wl = new User();
        wl.setUsername("wl");
        wl.setPassword("wl");
        userService.create(wl);

        User wd = new User();
        wd.setUsername("wd");
        wd.setPassword("wd");
        userService.create(wd);
        Role adminUser = new Role();
        adminUser.setRole(RoleConstant.ROLE_USER);
        adminUser.setUsers(Collections.singletonList(userService.findById(1L)));
        roleService.save(adminUser);

        Role admin = new Role();
        admin.setRole(RoleConstant.ROLE_ADMIN);
        admin.setUsers(Collections.singletonList(userService.findById(1L)));
        roleService.save(admin);

        Role u1 = new Role();
        u1.setRole(RoleConstant.ROLE_USER);
        u1.setUsers(Collections.singletonList(userService.findById(2L)));
        roleService.save(u1);

        Role u2 = new Role();
        u2.setRole(RoleConstant.ROLE_USER);
        u2.setUsers(Collections.singletonList(userService.findById(3L)));
        roleService.save(u2);

        friendService.follow(1L, 1L);
        friendService.follow(2L, 1L);
        friendService.follow(3L, 1L);

        friendService.follow(1L, 2L);
        friendService.follow(2L, 2L);
        friendService.follow(3L, 2L);

        friendService.follow(1L, 3L);
        friendService.follow(2L, 3L);
        friendService.follow(3L, 3L);

        Post post1 = new Post();
        post1.setAuthor(userService.findById(1L));
        post1.setContent("Hello,天码营!");
        postService.send(post1.getAuthor(), PostVO.toVO(post1));

        Post post2 = new Post();
        post2.setAuthor(userService.findById(2L));
        post2.setContent("Hello,World!");
        postService.send(post2.getAuthor(), PostVO.toVO(post2));

        Post post3 = new Post();
        post3.setAuthor(userService.findById(3L));
        post3.setContent("Hello,www.tianmaying.com");
        postService.send(post3.getAuthor(), PostVO.toVO(post3));

    }
}
