package com.xingfly.config;

import com.xingfly.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by SuperS on 30/05/2017.
 *
 * @author SuperS
 */
@EnableScheduling
@Configuration
public class SchedulingConfig {
    @Autowired
    private NotificationService notificationService;
    //每个星期三凌晨3点 删除 过期 的 提醒
    @Scheduled(cron = "0 0 3 ? * WED")
    public void deleteNotifications() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - 7);
        Date date = calendar.getTime();
        notificationService.deleteNotificationsByCreateTimeBefore(date);
    }
}
