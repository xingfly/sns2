package com.xingfly.web.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 18/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
@Builder
public class ResponseModel<T> {
    private Integer code;
    private String message;
    private T data;

    @Override
    public String toString() {
        return "ResponseModel{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }


}
