package com.xingfly.web.api;

import com.xingfly.authentication.annotation.Authorization;
import com.xingfly.authentication.annotation.Me;
import com.xingfly.constant.ApiConstant;
import com.xingfly.constant.ErrorConstant;
import com.xingfly.constant.HttpStatus;
import com.xingfly.error.TipException;
import com.xingfly.model.Friend;
import com.xingfly.model.User;
import com.xingfly.service.FriendService;
import com.xingfly.service.UserService;
import com.xingfly.web.model.ResponseModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;

/**
 * Created by SuperS on 23/05/2017.
 *
 * @author SuperS
 */
@Api(value = "friends", description = "用户关系相关")
@RestController
@RequestMapping("/api/friends")
public class FriendController {
    @Autowired
    private FriendService friendService;
    @Autowired
    private UserService userService;

    @ApiOperation(value = "关注用户操作", notes = "验证后关注某个用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "authorization", value = ApiConstant.AUTHORIZATION, paramType = "header", dataType = "String", required = true)
    })
    @PostMapping("{id}")
    @Authorization
    public ResponseModel follow(@PathVariable("id") Long id, @ApiIgnore @Me User me) throws TipException {
        User start = userService.findById(id);
        //用户不存在
        if (start == null) {
            throw new TipException(ErrorConstant.ERROR_MSG_USER_NOT_FOUND, ErrorConstant.ERROR_CODE_USER_NOT_FOUND);
        }
        //已经关注
        if (friendService.isFollow(me.getId(), id)) {
            throw new TipException(ErrorConstant.ERROR_MSG_FRIEND_IS_FOLLOW, ErrorConstant.ERROR_CODE_FRIEND_IS_FOLLOW);
        }
        Friend friend = new Friend();
        friend.setCreateTime(new Date());
        friend.setFans(me);
        friend.setStart(start);
        friendService.save(friend);
        return ResponseModel.builder().message(HttpStatus.STATUS_INFO_CREATED).code(HttpStatus.STATUS_CODE_CREATED).build();
    }

    @ApiOperation(value = "取关用户操作", notes = "验证后取关某个用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "authorization", value = ApiConstant.AUTHORIZATION, paramType = "header", dataType = "String", required = true)
    })
    @DeleteMapping("{id}")
    @Authorization
    @Transactional
    public ResponseModel unFollow(@PathVariable("id") Long id, @ApiIgnore @Me User me) throws TipException {
        User start = userService.findById(id);
        //用户不存在
        if (start == null) {
            throw new TipException(ErrorConstant.ERROR_MSG_USER_NOT_FOUND, ErrorConstant.ERROR_CODE_USER_NOT_FOUND);
        }
        //取关自己 不允许
        if (id.equals(me.getId())) {
            throw new TipException(ErrorConstant.ERROR_MSG_ERROR_OPTION, ErrorConstant.ERROR_CODE_ERROR_OPTION);
        }
        //未关注用户
        if (!friendService.isFollow(me.getId(), id)) {
            throw new TipException(ErrorConstant.ERROR_MSG_FRIEND_NOT_FOLLOW, ErrorConstant.ERROR_CODE_FRIEND_NOT_FOLLOW);
        }
        friendService.unFollow(me.getId(), id);
        return ResponseModel.builder().message(HttpStatus.STATUS_INFO_SUCCESS).code(HttpStatus.STATUS_CODE_SUCCESS).build();
    }
}
