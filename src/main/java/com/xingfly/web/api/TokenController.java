package com.xingfly.web.api;

import com.xingfly.authentication.annotation.Authorization;
import com.xingfly.authentication.annotation.Me;
import com.xingfly.constant.ApiConstant;
import com.xingfly.constant.ErrorConstant;
import com.xingfly.constant.HttpStatus;
import com.xingfly.error.TipException;
import com.xingfly.model.TokenModel;
import com.xingfly.model.User;
import com.xingfly.model.dto.AuthorizationDTO;
import com.xingfly.service.RedisTokenManager;
import com.xingfly.service.UserService;
import com.xingfly.web.model.ResponseModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by SuperS on 19/05/2017.
 * token-用户登录
 *
 * @author SuperS
 */
@RestController
@RequestMapping("/api/tokens")
//API名和API简介
@Api(value = "tokens", description = "用户Token相关API")
public class TokenController {
    //注入用户服务类
    @Autowired
    private UserService userService;
    //注入Token管理类
    @Autowired
    private RedisTokenManager redisTokenManager;

    //API操作简介
    @ApiOperation(value = "获取Token", notes = "用户登录")
    //API参数信息
    @ApiImplicitParam(name = "authorizationDTO", value = "登录信息", dataType = "AuthorizationDTO")
    @PostMapping
    public ResponseModel login(@RequestBody AuthorizationDTO authorizationDTO, @ApiIgnore HttpServletRequest request) throws TipException {
        if (StringUtils.isEmpty(authorizationDTO.getUsername()) || authorizationDTO.getPassword() == null) {
            throw new TipException(ErrorConstant.ERROR_MSG_USER_INFO_NOT_NULL, ErrorConstant.ERROR_CODE_USER_INFO_NOT_NULL);
        }
        String encryptPassword = DigestUtils.md5DigestAsHex(authorizationDTO.getPassword().getBytes());
        User user = userService.findByUserNameAndPassWord(authorizationDTO.getUsername(), encryptPassword);
        if (user == null) {
            throw new TipException(ErrorConstant.ERROR_MSG_LOGIN_ERROR, ErrorConstant.ERROR_CODE_LOGIN_ERROR);
        }
        TokenModel model = redisTokenManager.createToken(user.getId(), request.getSession().getId());
        return ResponseModel.builder().data(model).code(HttpStatus.STATUS_CODE_SUCCESS).message(HttpStatus.STATUS_INFO_SUCCESS).build();
    }

    @ApiOperation(value = "删除Token", notes = "用户退出(从request attribute中获取当前用户ID)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = ApiConstant.AUTHORIZATION, paramType = "header", dataType = "String", required = true)
    })
    @DeleteMapping
    @Authorization
    public ResponseModel logout(@ApiIgnore @Me User user) {
        redisTokenManager.deleteToken(user.getId());
        return ResponseModel.builder().code(HttpStatus.STATUS_CODE_SUCCESS).message(HttpStatus.STATUS_INFO_SUCCESS).build();
    }
}
