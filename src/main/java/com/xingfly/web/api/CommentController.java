package com.xingfly.web.api;

import com.xingfly.authentication.annotation.Authorization;
import com.xingfly.authentication.annotation.Me;
import com.xingfly.constant.ApiConstant;
import com.xingfly.constant.ErrorConstant;
import com.xingfly.constant.HttpStatus;
import com.xingfly.error.TipException;
import com.xingfly.model.Comment;
import com.xingfly.model.Event;
import com.xingfly.model.User;
import com.xingfly.model.dto.CommentInputDTO;
import com.xingfly.model.vo.CommentVO;
import com.xingfly.service.CommentService;
import com.xingfly.service.EventService;
import com.xingfly.web.model.ResponseModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by SuperS on 23/05/2017.
 *
 * @author SuperS
 */
@Api(value = "comments", description = "评论相关操作")
@RestController
@RequestMapping("/api/comments")
public class CommentController {
    //评论服务
    @Autowired
    private CommentService commentService;
    //事件服务
    @Autowired
    private EventService eventService;

    //API注解是Swagger2的文档
    @ApiOperation(value = "创建评论", notes = "验证后创建评论")
    @ApiImplicitParams({
            //Swagger2中文档里的参数
            @ApiImplicitParam(name = "authorization", value = ApiConstant.AUTHORIZATION, paramType = "header", dataType = "String", required = true),
            @ApiImplicitParam(name = "commentDTO", value = "parentId不传入是评论,传入是子评论", dataType = "CommentInputDTO", required = true)
    })
    //通过Post方式访问请求
    @PostMapping
    //此请求需要认证用户身份
    @Authorization
    //开启事务
    @Transactional
    public ResponseModel createComment(@ApiIgnore @Me User me, @RequestBody CommentInputDTO commentDTO) throws TipException {
        //根据 数据传输对象传入的 objID和objType来查找评论的事件是否存在
        Event event = eventService.findByObjIdAndType(commentDTO.getObjId(), commentDTO.getObjType());
        //评论对象不存在
        if (event == null) {
            throw new TipException(ErrorConstant.ERROR_MSG_EVENT_NOT_FOUND, ErrorConstant.ERROR_CODE_EVENT_NOT_FOUND);
        }
        //如果传入父评论ID 那么就认为是 Reply
        if (commentDTO.getParentId() != null) {
            //查询回复的评论是否存在
            Comment parentComment = commentService.findById(commentDTO.getParentId());
            if (parentComment == null) {
                //找不到该父评论
                throw new TipException(ErrorConstant.ERROR_MSG_COMMENT_NOT_FOUND, ErrorConstant.ERROR_CODE_COMMENT_NOT_FOUND);
            }
            //评论事件不一致不能发表回复
            if (!parentComment.getEvent().getId().equals(event.getId())) {
                throw new TipException(ErrorConstant.ERROR_MSG_COMMENT_EVENT_ERROR, ErrorConstant.ERROR_CODE_COMMENT_EVENT_ERROR);
            }
        }
        //发布评论 send中会调用notificationSender 通知对方
        Comment saved = commentService.send(me, commentDTO);
        return ResponseModel.builder().message(HttpStatus.STATUS_INFO_CREATED).code(HttpStatus.STATUS_CODE_CREATED).data(CommentVO.toVoIgnoreReplies(saved)).build();
    }

    @ApiOperation(value = "删除评论", notes = "验证后删除评论")
    @DeleteMapping("{id}")
    @Authorization
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authorization", value = ApiConstant.AUTHORIZATION, paramType = "header", dataType = "String", required = true),
            @ApiImplicitParam(name = "id", required = true, dataType = "Long", paramType = "path")
    })
    @Transactional
    public ResponseModel deleteComment(@PathVariable("id") Long id, @ApiIgnore @Me User me) throws TipException {
        //判断需要删除的评论作者是不是当前登录用户
        if (!commentService.findById(id).getAuthor().getId().equals(me.getId())) {
            throw new TipException(ErrorConstant.ERROR_MSG_USER_NOT_MATCH, ErrorConstant.ERROR_CODE_USER_NOT_MATCH);
        }
        //如果评论有子评论 那么 删除子评论
        if (commentService.countSonsComment(id) > 0) {
            commentService.deleteSonsCommentByParentId(id);
        }
        //删除评论
        commentService.deleteById(id);
        return ResponseModel.builder().message(HttpStatus.STATUS_INFO_SUCCESS).code(HttpStatus.STATUS_CODE_SUCCESS).build();
    }
}