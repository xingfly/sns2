package com.xingfly.error;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by SuperS on 19/05/2017.
 *
 * @author SuperS
 */
@Getter
@Setter
@Builder
public class ErrorResponseModel<T> {
    private int code;
    private String message;
    private T data;
    private String url;

    @Override
    public String toString() {
        return "ErrorResponseModel{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", url='" + url + '\'' +
                '}';
    }
}
