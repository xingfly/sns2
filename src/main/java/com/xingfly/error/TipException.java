package com.xingfly.error;

/**
 * Created by SuperS on 14/05/2017.
 *
 * @author SuperS
 */
public class TipException extends Exception {
    private Integer code;

    public TipException(String msg, Integer code) {
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
