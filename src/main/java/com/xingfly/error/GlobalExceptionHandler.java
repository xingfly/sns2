package com.xingfly.error;

import com.xingfly.constant.ErrorConstant;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 全局错误拦截
 * Created by SuperS on 19/05/2017.
 *
 * @author SuperS
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    //用户操作错误处理器
    @ExceptionHandler(value = TipException.class)
    @ResponseBody
    public ErrorResponseModel jsonErrorHandler(TipException e, HttpServletRequest request) {
        //拦截错误提示
        return ErrorResponseModel.builder()
                .code(e.getCode())
                .message(e.getMessage())
                .url(request.getRequestURL().toString()).build();

    }

    @ExceptionHandler(value = NumberFormatException.class)
    @ResponseBody
    public ErrorResponseModel jsonErrorHandler(NumberFormatException e, HttpServletRequest request) {
        //拦截错误提示
        return ErrorResponseModel.builder()
                .code(ErrorConstant.ERROR_CODE_NUMBER_FORMAT_ERROR)
                .message(ErrorConstant.ERROR_MSG_NUMBER_FORMAT_EXCEPTION)
                .url(request.getRequestURL().toString()).build();
    }

}
