package com.xingfly.service;

import com.xingfly.model.Event;
import com.xingfly.model.Image;
import com.xingfly.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SuperS on 18/05/2017.
 *
 * @author SuperS
 */
@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;

    //根据EventID查询 图片列表
    public List<Image> findByEventId(Long id) {
        return imageRepository.findByEventId(id);
    }
    //根据图片id查询图片
    public Image findById(Long id) {
        return imageRepository.findOne(id);
    }
    //根据图片hash查询图片对象(hash有重复时选择第一个)
    public Image findByHashTop1(String hash) {
        return imageRepository.findTop1ByHash(hash);
    }
    //统计图片数量根据图片hash
    public Long countImagesByHash(String hash) {
        return imageRepository.countImagesByHash(hash);
    }
    //删除某个用户的所有图片
    public boolean deleteImagesByUserId(Long uid) {
        Long all = imageRepository.countImagesByUserId(uid);
        if (all > 0) {
            imageRepository.deleteImagesByUserId(uid);
            return true;
        }
        return false;
    }
    //删除某个Event的所有图片
    public boolean deleteAllByEvent(Event event) {
        if (imageRepository.countImagesByEventsContains(event) > 0) {
            imageRepository.removeImagesByEventsContains(event);
        }
        return false;
    }
    //保存图片
    public Image save(Image image) {
        return imageRepository.save(image);
    }
}
