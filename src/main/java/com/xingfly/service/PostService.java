package com.xingfly.service;

import com.xingfly.constant.Type;
import com.xingfly.model.Event;
import com.xingfly.model.Post;
import com.xingfly.model.User;
import com.xingfly.model.dto.EventImageDTO;
import com.xingfly.model.vo.PostVO;
import com.xingfly.mq.EventImageRelationSender;
import com.xingfly.mq.FeedSender;
import com.xingfly.mq.NotificationSender;
import com.xingfly.repository.EventRepository;
import com.xingfly.repository.PostRepository;
import com.xingfly.util.SummaryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private NotificationSender notificationSender;
    @Autowired
    private FeedSender feedSender;
    @Autowired
    private EventImageRelationSender eventImageRelationSender;
    //根据id查找Post
    public Post findById(Long id) {
        return postRepository.findOne(id);
    }
    //创建post
    public Post save(Post post) {
        return postRepository.save(post);
    }


    @Transactional
    public Post send(User author, PostVO postVO) {
        //创建Post
        Post post = new Post();
        post.setAuthor(author);
        post.setCreateTime(postVO.getCreateTime());
        post.setContent(postVO.getContent());
        Post p = postRepository.save(post);
        //创建时间包装post
        Event e = new Event();
        e.setEventObjectId(p.getId());
        //设置事件简介
        e.setSummary(SummaryUtil.summary(postVO.getContent()));
        e.setUser(author);
        //设置事件类型
        e.setType(Type.EVENT_POST);
        //保存事件
        Event event = eventRepository.save(e);
        //将事件发送给notification消息队列 通知所有粉丝
        notificationSender.send(event);
        //将事件发送给feed消息队列 在所有粉丝的feed流中插入一个新的feed
        feedSender.send(event);
        //如果post包含图片 创建 event和图片的关系
        if (postVO.getImageVOS() != null && !postVO.getImageVOS().isEmpty()) {
            EventImageDTO eventImageDTO = new EventImageDTO();
            eventImageDTO.setEventId(event.getId());
            eventImageDTO.setImageVOS(postVO.getImageVOS());
            eventImageRelationSender.send(eventImageDTO);
        }
        return p;
    }

    //删除某个用户的所有posts
    public boolean deleteAllByUserId(Long uid) {
        Long all = postRepository.countPostsByAuthorId(uid);
        if (all > 0) {
            postRepository.deletePostsByAuthorId(uid);
            return true;
        }
        return false;
    }

    //分页获取Post
    public Page<Post> findPostsByUserId(Long uid, Pageable pageable) {
        return postRepository.findPostsByAuthorId(uid, pageable);
    }

    //获取posts的数量
    public Long countPostsByUserId(Long id) {
        return postRepository.countPostsByAuthorId(id);
    }
    //根据postID删除post
    public void deleteById(Long id) {
        postRepository.delete(id);
    }
}
