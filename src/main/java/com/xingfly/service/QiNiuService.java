package com.xingfly.service;

import com.qiniu.util.Auth;
import org.springframework.stereotype.Service;

/**
 * Created by SuperS on 24/05/2017.
 *
 * @author SuperS
 */
@Service
public class QiNiuService {
    public Auth getToken(String accessKey, String secretKey) {
        return Auth.create(accessKey, secretKey);
    }
}
