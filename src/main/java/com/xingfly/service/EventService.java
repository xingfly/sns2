package com.xingfly.service;

import com.xingfly.model.Event;
import com.xingfly.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SuperS on 18/05/2017.
 *
 * @author SuperS
 */
@Service
public class EventService {
    @Autowired
    private EventRepository eventRepository;
    //删除某个用户所有的事件
    public boolean deleteAllByUserId(Long uid) {
        Long all = eventRepository.countEventsByUserId(uid);
        if (all > 0) {
            eventRepository.deleteEventsByUserId(uid);
            return true;
        }
        return false;
    }

    //查找事件通过对象ID和对象类型
    public Event findByObjIdAndType(Long objId, Integer type) {
        return eventRepository.findByEventObjectIdAndType(objId, type);
    }

    //删除所有事件通过对象id和对象类型
    public boolean deleteAllByEventObjectIdAndType(Long objId, Integer type) {
        if (eventRepository.countEventsByEventObjectId(objId) > 0) {
            eventRepository.deleteEventByEventObjectIdAndType(objId, type);
        }
        return false;
    }
    //通过事件id查询事件
    public Event findByEventId(Long eventId) {
        return eventRepository.findOne(eventId);
    }


}
