package com.xingfly.service;

import com.xingfly.model.Count;
import com.xingfly.repository.CountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SuperS on 16/05/2017.
 *
 * @author SuperS
 */
@Service
public class CountService {
    @Autowired
    private CountRepository countRepository;
    //判断用户是否已经点赞了某个操作
    public boolean isLike(Long eventId, Long userId) {
        Count count = countRepository.findByEventIdAndUserId(eventId, userId);
        return count != null;
    }
    //根据事件ID查询Count对象
    public Count findByEventId(Long eventId) {
        return countRepository.findByEventId(eventId);
    }
    //保存Count对象
    public Count save(Count count) {
        return countRepository.save(count);
    }
}
