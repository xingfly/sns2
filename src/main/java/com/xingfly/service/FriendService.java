package com.xingfly.service;

import com.xingfly.model.Friend;
import com.xingfly.repository.FriendRepository;
import com.xingfly.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
@Service
public class FriendService {
    @Autowired
    private FriendRepository friendRepository;
    @Autowired
    private UserRepository userRepository;

    //关注->创建用户的关系
    @Transactional
    public boolean follow(Long userId, Long followUserId) {
        Friend friend = friendRepository.findByStartIdAndFansId(followUserId, userId);
        if (friend == null) {
            Friend fans = new Friend();
            fans.setStart(userRepository.findOne(followUserId));
            fans.setFans(userRepository.findOne(userId));
            friendRepository.save(fans);
            return true;
        }
        return false;
    }
    //取关->取消用户的关系
    @Transactional
    public void unFollow(Long fansId, Long startId) {
        friendRepository.deleteByFansIdAndStartId(fansId, startId);
        friendRepository.deleteByFansIdAndStartId(startId, fansId);
    }

    //判断是不是相互关注
    @Transactional
    public boolean isFriend(Long user1Id, Long user2Id) {
        Friend friend1 = friendRepository.findByStartIdAndFansId(user1Id, user2Id);
        Friend friend2 = friendRepository.findByStartIdAndFansId(user2Id, user1Id);
        if (friend1 != null && friend2 != null) {
            return true;
        }
        return false;
    }

    //判断是否已经关注
    @Transactional
    public boolean isFollow(Long fansId, Long startId) {
        Friend friend = friendRepository.findByStartIdAndFansId(startId, fansId);
        return friend != null;
    }

    //删除跟着用户有关的所有关系 关注、被关注
    public boolean deleteAllByUserId(Long uid) {
        boolean flag = false;
        Long allFans = friendRepository.countFriendsByFansId(uid);
        Long allStarts = friendRepository.countFriendsByStartId(uid);
        if (allFans > 0) {
            friendRepository.deleteFriendsByFansId(uid);
            flag = true;
        }
        if (allStarts > 0) {
            friendRepository.deleteFriendsByStartId(uid);
            flag = true;
        }
        return flag;
    }
    //分页查找用户的粉丝
    public Page<Friend> findUserFansByUserId(Long uid, Pageable pageable) {
        return friendRepository.findFriendsByStartId(uid, pageable);
    }
    //分页查找用户的关注者
    public Page<Friend> findUserStartsByUserId(Long uid, Pageable pageable) {
        return friendRepository.findFriendsByFansId(uid, pageable);
    }
    //统计粉丝根据用户id
    public Long countFans(Long uid) {
        //去除自己
        return friendRepository.countFriendsByStartId(uid) - 1;
    }
    //统计关注者根据用户id
    public Long countStats(Long uid) {
        //去除自己
        return friendRepository.countFriendsByFansId(uid) - 1;
    }
    //创建Friend对象
    public void save(Friend friend) {
        friendRepository.save(friend);
    }
}
