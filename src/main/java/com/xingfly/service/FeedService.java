package com.xingfly.service;

import com.xingfly.model.Event;
import com.xingfly.repository.FeedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by SuperS on 17/05/2017.
 *
 * @author SuperS
 */
@Service
public class FeedService {
    @Autowired
    private FeedRepository feedRepository;

    //根据粉丝id分页查询事件(事件包含了对应的Post)->相当于根据粉丝id查看他发表过什么内容
    public Page<Event> findEventsByFansId(Long fans_id, Pageable pageable) {
        return feedRepository.findEventsByFansId(fans_id, pageable);
    }
    //根据作者id分页查询事件(事件包含了对应的Post)->相当于根据作者id查看他发表过什么内容
    public Page<Event> findEventsByAuthorId(Long author_id, Pageable pageable) {
        return feedRepository.findEventsByAuthorId(author_id, pageable);
    }
    //根据作者id删除所有的Feed对象
    public boolean deleteAllByAuthorId(Long uid) {
        boolean flag = false;
        Long allAuthorFeed = feedRepository.countFeedsByAuthorId(uid);
        Long allFansFeed = feedRepository.countFeedsByFansId(uid);
        if (allFansFeed > 0) {
            feedRepository.deleteFeedsByFansId(uid);
            flag = true;
        }
        if (allAuthorFeed > 0) {
            feedRepository.deleteFeedsByAuthorId(uid);
            flag = true;
        }
        return flag;
    }
    //根据事件id删除所有的feed对象
    public boolean deleteAllByEventId(Long eid) {
        if (feedRepository.countFeedsByEventId(eid) > 0) {
            feedRepository.deleteFeedsByEventId(eid);
            return true;
        }
        return false;
    }
}

