package com.xingfly.service;

import com.xingfly.constant.Type;
import com.xingfly.model.Comment;
import com.xingfly.model.Event;
import com.xingfly.model.User;
import com.xingfly.model.dto.CommentInputDTO;
import com.xingfly.mq.NotificationSender;
import com.xingfly.repository.CommentRepository;
import com.xingfly.repository.EventRepository;
import com.xingfly.util.SummaryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by SuperS on 18/05/2017.
 *
 * @author SuperS
 */
@Service
public class CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private NotificationSender notificationSender;

    public Long countSonsComment(Long parentId) {
        return commentRepository.countCommentsByParentId(parentId);
    }

    public Long countCommentsByEventId(Long eventId) {
        return commentRepository.countCommentsByEventId(eventId);
    }

    public List<Comment> findByEventIdAndEventType(Long id, int type) {
        return commentRepository.findByEventIdAndEventType(id, type);
    }

    public List<Comment> findByEventIdAndParentId(Long id, Long pid) {
        return commentRepository.findByEventIdAndParentId(id, pid);
    }

    public boolean deleteAllByUserId(Long uid) {
        Long all = commentRepository.countCommentsByAuthorId(uid);
        if (all > 0) {
            commentRepository.deleteCommentsByAuthorId(uid);
            return true;
        }
        return false;
    }

    @Transactional
    public Comment send(User author, CommentInputDTO commentInputDTO) {
        Comment comment = new Comment();
        Event event = new Event();
        comment.setEvent(eventRepository.findByEventObjectIdAndType(commentInputDTO.getObjId(),commentInputDTO.getObjType()));
        comment.setAuthor(author);
        comment.setContent(commentInputDTO.getContent());
        if (commentInputDTO.getParentId() != null) {
            comment.setParentId(commentInputDTO.getParentId());
            comment.setType(Type.COMMENT_REPLY);
            event.setType(Type.EVENT_REPLY);
        } else {
            comment.setType(Type.COMMENT_COMMENT);
            event.setType(Type.EVENT_COMMENT);
        }
        Comment c = commentRepository.save(comment);
        event.setEventObjectId(c.getId());
        event.setSummary(SummaryUtil.summary(commentInputDTO.getContent()));
        event.setUser(author);
        Event e = eventRepository.save(event);
        notificationSender.send(e);
        return c;
    }

    public Comment save(Comment comment) {
        return commentRepository.save(comment);
    }

    public Comment findById(Long id) {
        return commentRepository.findOne(id);
    }

    public void deleteById(Long id) {
        commentRepository.delete(id);
    }

    public void deleteSonsCommentByParentId(Long parentId) {
        commentRepository.removeCommentsByParentId(parentId);
    }
}
