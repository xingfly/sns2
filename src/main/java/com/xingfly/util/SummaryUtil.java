package com.xingfly.util;

/**
 * 负责把用户的传入的内容缩短
 * Created by SuperS on 2017/7/19.
 *
 * @author SuperS
 */
public class SummaryUtil {
    private final static int DEFAULT_SIZE = 10;
    private final static String DEFAULT_STR = "...";

    public static String summary(String str) {
        if (str.length() < DEFAULT_SIZE) {
            return str + DEFAULT_STR;
        }
        return str.substring(0, DEFAULT_SIZE) + DEFAULT_STR;
    }

    public static String summary(String str, Integer size) {
        if (str.length() < size) {
            return str + DEFAULT_STR;
        }
        return str.substring(0, size) + DEFAULT_STR;
    }
}
