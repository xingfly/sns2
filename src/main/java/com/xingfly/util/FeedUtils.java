package com.xingfly.util;

import com.xingfly.constant.Type;
import com.xingfly.model.*;
import com.xingfly.model.vo.*;
import com.xingfly.service.CommentService;
import com.xingfly.service.CountService;
import com.xingfly.service.ImageService;
import com.xingfly.service.PostService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SuperS on 22/05/2017.
 *
 * @author SuperS
 */
public class FeedUtils {
    //组装feedVO
    public static List<FeedVO> assemblingFeedVO(List<Event> events,
                                                PostService postService,
                                                CountService countService,
                                                ImageService imageService,
                                                CommentService commentService) {
        //创建FeedVO 列表
        List<FeedVO> feedVOS = new ArrayList<>();
        //根据传入的event列表数据组装feed流
        for (Event event : events) {
            //获取事件对象中包含的post(这里的events是从FeedService中获取的所以保证了所有Event都是Post不会是Comment)
            Post post = postService.findById(event.getEventObjectId());
            //创建FeedVO
            FeedVO feedVO = new FeedVO();
            feedVO.setFeedEventId(event.getId());
            feedVO.setAuthor(UserVO.toVO(event.getUser()));
            feedVO.setContent(post.getContent());
            feedVO.setCreateTime(post.getCreateTime());
            feedVO.setId(event.getEventObjectId());
            //查询该事件的like数据
            Count count = countService.findByEventId(event.getId());
            CountVO countVO = new CountVO();
            //如果存在like数据
            if (count != null) {
                //组装countVO
                countVO.setId(count.getId());
                countVO.setLikeCount(count.getCount());
                List<User> users = count.getUsers();
                List<UserVO> userVOS = new ArrayList<>();
                if (users != null) {
                    for (User u : users) {
                        userVOS.add(UserVO.toVO(u));
                    }
                }
                countVO.setUsers(userVOS);
                feedVO.setCountVO(countVO);
            }
            List<FeedImageVO> feedImageVOS = new ArrayList<>();
            //获取事件关联的图片数据
            List<Image> images = imageService.findByEventId(event.getId());
            if (images != null) {
                //如果存在就组装ImageVO
                for (Image image : images) {
                    FeedImageVO feedImageVO = new FeedImageVO();
                    feedImageVO.setUrl(image.getUrl());
                    feedImageVOS.add(feedImageVO);
                }
                feedVO.setImages(feedImageVOS);
            }

            List<CommentVO> commentVOS = new ArrayList<>();
            List<Comment> comments = commentService.findByEventIdAndEventType(event.getId(),
                    Type.COMMENT_COMMENT);
            if (comments != null) {
                for (Comment comment : comments) {
                    CommentVO commentVO = new CommentVO();
                    commentVO.setCreateTime(comment.getCreateTime());
                    commentVO.setContent(comment.getContent());
                    commentVO.setId(comment.getId());
                    commentVO.setType(comment.getType());
                    commentVO.setAuthor(UserVO.toVO(comment.getAuthor()));
                    //组装Reply数据
                    List<CommentVO> replies = new ArrayList<>();
                    List<Comment> replyList = commentService.findByEventIdAndParentId(event.getId(),
                            comment.getId());
                    if (replyList != null) {
                        for (Comment reply : replyList) {
                            CommentVO replyVO = new CommentVO();
                            replyVO.setCreateTime(reply.getCreateTime());
                            replyVO.setAuthor(UserVO.toVO(comment.getAuthor()));
                            replyVO.setContent(reply.getContent());
                            replyVO.setId(reply.getId());
                            replyVO.setType(reply.getType());
                            replies.add(replyVO);
                        }
                        commentVO.setReplies(replies);
                        commentVOS.add(commentVO);
                    }
                }
                //设置评论数据
                feedVO.setComments(commentVOS);
            }
            feedVOS.add(feedVO);
        }
        return feedVOS;
    }
}
