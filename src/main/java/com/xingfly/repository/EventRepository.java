package com.xingfly.repository;

import com.xingfly.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SuperS on 13/05/2017.
 *
 * @author SuperS
 */
public interface EventRepository extends JpaRepository<Event, Long> {
    //删除当前用户id的所有事件
    void deleteEventsByUserId(Long uid);
    //统计当前用户id的所有事件
    Long countEventsByUserId(Long uid);
    //统计某个对象id的所有事件
    Long countEventsByEventObjectId(Long objId);
    //查找事件 根据 对象的id 和 对象的类型
    Event findByEventObjectIdAndType(Long objId, Integer type);
    //删除事件 根据 对象的id 和 对象的类型
    void deleteEventByEventObjectIdAndType(Long objId, Integer type);
}
