package com.xingfly.repository;

import com.xingfly.model.Event;
import com.xingfly.model.Feed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
public interface FeedRepository extends JpaRepository<Feed, Long> {
    //根据粉丝id分页查询Feed对象
    Page<Feed> findByFansId(Long fans_id, Pageable pageable);
    //根据粉丝ID分页查询事件 -> 相当于根据用户id查询发表的post
    @Query("select f.event from Feed f where f.fans.id=:id")
    Page<Event> findEventsByFansId(@Param("id") Long fans_id, Pageable pageable);
    //根据关注id分页查询事件
    @Query("select f.event from Feed f where f.author.id=:id and f.fans.id=:id")
    Page<Event> findEventsByAuthorId(@Param("id") Long stars_id, Pageable pageable);
    //根据用户id删除所有feeds
    void deleteFeedsByAuthorId(Long uid);
    //根据事件id删除所有feeds
    void deleteFeedsByEventId(Long eid);
    //根据粉丝id删除所有feeds
    void deleteFeedsByFansId(Long uid);
    //统计feeds数量根据用户id
    Long countFeedsByAuthorId(Long uid);
    //统计feeds数量根据事件id
    Long countFeedsByEventId(Long eid);
    //统计feeds数量根据粉丝id
    Long countFeedsByFansId(Long uid);
}
