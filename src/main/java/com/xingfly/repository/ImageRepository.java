package com.xingfly.repository;

import com.xingfly.model.Event;
import com.xingfly.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

    //根据事件ID查找所有事件
    @Query("select i from Image i join i.events e where e.id=:id")
    List<Image> findByEventId(@Param("id") Long id);
    //根据用户id删除所有图片
    void deleteImagesByUserId(Long id);
    //根据Event对象删除所有图片对象
    void removeImagesByEventsContains(Event event);
    //统计图片数量根据用户id
    Long countImagesByUserId(Long id);
    //统计图片数量根据事件对象
    Long countImagesByEventsContains(Event event);
    //根据hash查找图片 选择 第一个(如果有很多一样的hash时)
    Image findTop1ByHash(String hash);
    //统计图片数量根据hash
    Long countImagesByHash(String hash);
}
