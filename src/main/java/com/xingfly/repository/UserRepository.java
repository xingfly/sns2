package com.xingfly.repository;

import com.xingfly.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
public interface UserRepository extends JpaRepository<User, Long> {
    //根据用户名和用户密码查找用户对象 用于登录
    User findByUsernameAndPassword(String name, String password);
    //根据用户名查找用户对象
    User findByUsername(String username);
}
