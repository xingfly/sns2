package com.xingfly.repository;

import com.xingfly.model.Role;
import com.xingfly.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by SuperS on 21/05/2017.
 *
 * @author SuperS
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    @Query("select r from Role r join r.users u where u.id=:id")
    List<Role> findRolesByUserId(@Param("id") Long id);

//    @Modifying
//    @Query("delete from Role r where r.users")
//    @Transactional
//    void deleteRolesByUserId(@Param("id") Long uid);

    void removeRolesByUsersContains(User user);

    @Query("select count(r) from Role r join r.users u where u.id=:id")
    Long countRolesByUserId(@Param("id") Long uid);

}
