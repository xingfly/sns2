package com.xingfly.repository;

import com.xingfly.model.Friend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
public interface FriendRepository extends JpaRepository<Friend, Long> {
    //根据关注者和被关注者查询Friend对象
    Friend findByStartIdAndFansId(Long start_id, Long fans_id);
    //查询所有粉丝id
    @Query("select f.fans.id from Friend f where f.start.id=:id")
    List<Long> findByUserAllFansId(@Param("id") Long user_id);
    //查询所有关注人的id
    @Query("select f.start.id from Friend  f where f.fans.id=:id")
    List<Long> findByUserAllStartsId(@Param("id") Long user_id);

    //取关->删除Friend根据粉丝id和被关注者id
    @Modifying
    @Transactional
    @Query("delete from Friend f where f.fans.id=:fid and f.start.id=:sid")
    void deleteByFansIdAndStartId(@Param("fid") Long fans_id, @Param("sid") Long start_id);

    //根据粉丝id删除所有Friend
    void deleteFriendsByFansId(Long uid);
    //根据关注者id删除所有Friend
    void deleteFriendsByStartId(Long uid);
    //根据粉丝id统计所有关注数量
    Long countFriendsByFansId(Long uid);
    //根据关注者id统计所有粉丝数量
    Long countFriendsByStartId(Long uid);
    //分页查询 粉丝
    Page<Friend> findFriendsByStartId(Long uid, Pageable pageable);
    //分页查询 关注者
    Page<Friend> findFriendsByFansId(Long uid, Pageable pageable);
}
