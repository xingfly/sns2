package com.xingfly.repository;

import com.xingfly.model.Count;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by SuperS on 16/05/2017.
 *
 * @author SuperS
 */
public interface CountRepository extends JpaRepository<Count, Long> {
    //查询count 根据事件id和用户id
    @Query("select c from Count c join c.users u where c.event.id=:cid and u.id=:uid")
    Count findByEventIdAndUserId(@Param("cid") Long eventId, @Param("uid") Long userId);
    //查询count 根据事件id
    Count findByEventId(Long eventId);

}
