package com.xingfly.repository;

import com.xingfly.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
public interface PostRepository extends JpaRepository<Post, Long> {
    //根据用户id删除该用户所有post
    void deletePostsByAuthorId(Long uid);
    //根据用户id统计该用户所有post数量
    Long countPostsByAuthorId(Long uid);
    //根据用户id分页查询post数据
    Page<Post> findPostsByAuthorId(Long uid, Pageable pageable);
}
