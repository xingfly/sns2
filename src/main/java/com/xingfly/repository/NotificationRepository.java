package com.xingfly.repository;

import com.xingfly.model.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by SuperS on 12/05/2017.
 *
 * @author SuperS
 */
public interface NotificationRepository extends JpaRepository<Notification, Long> {
    //删除消息接受者的所有 消息提醒
    void deleteNotificationsByReceiverId(Long uid);
    //删除消息发送者的所有 消息提醒
    void deleteNotificationsBySenderId(Long uid);
    //统计消息数量根据接受者id
    Long countNotificationsByReceiverId(Long uid);
    //统计消息数量根据发送者id
    Long countNotificationsBySenderId(Long uid);
    //统计接受者未读或已读信息的数量
    @Query("select count(n) from Notification n where n.receiver.id=:uid and n.status=:status")
    Long countNotificationsByReceiverIdAndStatus(@Param("uid") Long receiver_id, @Param("status") Integer status);
    //分页查询消息的数量根据接受者的id
    Page<Notification> findNotificationsByReceiverId(Long id, Pageable pageable);
    //分页查询消息的数量根据接受者和状态(已读或者未读)
    Page<Notification> findNotificationsByReceiverIdAndStatus(Long id, Integer status, Pageable pageable);
    //统计消息的数量根据事件id
    Long countNotificationsByEventId(Long eid);
    //删除某个事件的所有消息
    void deleteNotificationsByEventId(Long eid);
    //删除某个时间点前的所有消息(用于定时清理Notification)
    @Modifying
    @Transactional
    @Query("delete from Notification n where n.createTime < :time")
    void deleteNotificationsByCreateTimeBefore(@Param("time") Date time);
}
