package com.xingfly.repository;

import com.xingfly.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by SuperS on 16/05/2017.
 *
 * @author SuperS
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByEventId(Long event_id);

    List<Comment> findByEventIdAndParentId(Long event_id, Long parent_id);

    @Query("select c from Comment c where c.event.id = :eid and c.type = :t")
    List<Comment> findByEventIdAndEventType(@Param("eid") Long event_id, @Param("t") Integer type);

    //删除某个用户所有评论
    void deleteCommentsByAuthorId(Long id);

    //查询某个用户所有评论的数量
    Long countCommentsByAuthorId(Long id);

    //查询某个Event的所有评论根据 事件 id
    Long countCommentsByEventId(Long eid);

    //查询某个父级评论的子评论个数
    Long countCommentsByParentId(Long parentId);

    //删除所有评论 根据 父评论 id
    void removeCommentsByParentId(Long parentId);
}
