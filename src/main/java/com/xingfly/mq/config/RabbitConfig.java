package com.xingfly.mq.config;

import com.xingfly.constant.MQConstant;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * 创建消息队列
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
@Configuration
public class RabbitConfig {
    @Bean
    public Queue notification() {
        return new Queue(MQConstant.NOTIFICATION_MQ);
    }

    @Bean
    public Queue feed() {
        return new Queue(MQConstant.FEED_MQ);
    }

    @Bean
    public Queue event(){
        return new Queue(MQConstant.EVENT_IMAGE_MQ);
    }
}
