package com.xingfly.mq;

import com.xingfly.constant.MQConstant;
import com.xingfly.constant.Type;
import com.xingfly.model.Comment;
import com.xingfly.model.Event;
import com.xingfly.model.Notification;
import com.xingfly.repository.FriendRepository;
import com.xingfly.repository.NotificationRepository;
import com.xingfly.repository.UserRepository;
import com.xingfly.service.CommentService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 消息接受者
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
@Component
//监听 MQ.NOTIFICATION_MQ这个队列
@RabbitListener(queues = MQConstant.NOTIFICATION_MQ)
public class NotificationReceiver {
    @Autowired
    private FriendRepository friendRepository;
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommentService commentService;

    //处理器 当接受到 发送者发送过来的Event时就执行process做响应的处理
    @RabbitHandler
    public void process(Event e) {
        //根据不同的事件类型做不同的处理
        switch (e.getType()) {
            //处理Post事件
            case Type.EVENT_POST:
                //获取事件创建者的所有粉丝。
                List<Long> fansId = friendRepository.findByUserAllFansId(e.getUser().getId());
                //遍历创建Notification
                for (Long id : fansId) {
                    Notification notification = new Notification();
                    notification.setType(Type.NOTIFICATION_POST);
                    notification.setReceiver(userRepository.findOne(id));
                    notification.setSender(e.getUser());
                    notification.setEvent(e);
                    notification.setCreateTime(new Date());
                    notificationRepository.save(notification);
                }
                break;
                //处理Comment事件
            case Type.EVENT_COMMENT:
                Notification notification = new Notification();
                //获取事件对应的Comment对象id
                Comment comment = commentService.findById(e.getEventObjectId());
                //comment有父评论 设置消息类型为 Reply
                if (comment.getParentId() != null) {
                    notification.setType(Type.NOTIFICATION_REPLY);
                } else {
                    //没有父评论设置消息类型为Comment
                    notification.setType(Type.NOTIFICATION_COMMENT);
                }
                //找到所评论的事件
                Event event = comment.getEvent();
                //设置消息接受者为此事件的作者
                notification.setReceiver(event.getUser());
                //设置消息发送者为评论的创建者
                notification.setSender(e.getUser());
                //设置消息的事件
                notification.setEvent(e);
                notification.setCreateTime(new Date());
                notificationRepository.save(notification);
                break;

        }
    }
}
