package com.xingfly.mq;

import com.xingfly.constant.MQConstant;
import com.xingfly.model.dto.EventImageDTO;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by SuperS on 24/05/2017.
 *
 * @author SuperS
 */
@Component
public class EventImageRelationSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;
    //发送EventImageDto数据传输对象到EVENT_IMAGE_MQ消息队列中
    public void send(EventImageDTO eventImageDTO) {
        rabbitTemplate.convertAndSend(MQConstant.EVENT_IMAGE_MQ, eventImageDTO);
    }
}
