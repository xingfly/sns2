package com.xingfly.mq;

import com.xingfly.constant.MQConstant;
import com.xingfly.model.Event;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息发送者
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
@Component
public class NotificationSender {
    //Amqp模板 负责 发送Event对象(包装了Post、Comment)到消息队列中
    @Autowired
    private AmqpTemplate rabbitTemplate;
    //发送event到对应的消息队列
    public void send(Event event) {
        rabbitTemplate.convertAndSend(MQConstant.NOTIFICATION_MQ, event);
    }
}
