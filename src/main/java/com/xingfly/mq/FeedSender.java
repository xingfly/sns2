package com.xingfly.mq;

import com.xingfly.constant.MQConstant;
import com.xingfly.model.Event;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by SuperS on 15/05/2017.
 *
 * @author SuperS
 */
@Component
public class FeedSender {
    @Autowired
    private AmqpTemplate rabbitTemplate;
    public void send(Event e) {
        //发送事件对象到FEED_MQ消息队列
        rabbitTemplate.convertAndSend(MQConstant.FEED_MQ, e);
    }
}
