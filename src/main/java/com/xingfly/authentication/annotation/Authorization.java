package com.xingfly.authentication.annotation;

import com.xingfly.constant.RoleConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by SuperS on 19/05/2017.
 *
 * @author SuperS
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization {
    String role() default RoleConstant.ROLE_USER;
}
